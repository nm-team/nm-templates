
Debian NM Process, documentation developers
===========================================

doc maintainers should prove beyond doubt that they can handle themselves at
something far better than short sentences, and disconnected, unfocused
ramblings with no paragraph breaks.  Remember that throughout the application
process, and make sure to keep it in account when analysing _all_ text they
produced (including the answers to other parts of the NM process than the tasks
& skills test).

You must know what to look for when 'grading' the tests below: if you feel
you cannot judge them fairly, then don't accept documentation NMs.

Also, do not forget to write the people the documentation NM has been working
with.  You should ask them how well they are doing, how good they are at
working in teams, and so on.


Tasks and Skills tests:

1.  SGML and sister technologies

	SGML is the cold reality of Debian documentation.  It does not matter
	how one feels about it, one has to learn the basics.  It is to
	documentation what POSIX shell scripting is to Debian packaging.

	SGML by itself is not enough.  The concept of stylesheets, of document
	processors that apply the sheets to SGML documents, and the scripting
	(or makefile) glue needed to automate this should also be important
	parts of this test.  So is DocBook.

	So far, the best test I came up with is:

	"Choose one Debian subsystem, and document it thoroughly.  You can
	choose one of three target audiences for your work:  novice user,
	advanced user, or Debian developer.  Be warned that the documentation
	for novice users must actually teach them the topic at hand _in depth_.

	"The document should be written using the SGML or XML DocBook DTD.  It
	should be transformed, using the toolset of your choice among those in
	Debian stable, into:  high quality PDF, high quality PostScript,
	W3C-compliant html document (in two variants: as a single large file,
	and as smaller hyperlinked files), and properly formatted plain text.

	"The document must include at least one table, at least one image or
	diagram, a TOC, an index, and one item of out-of-band information
	(footnote or margin note).  You are expected to make proper use of
	these: they must fit well within the whole document."

	Yes, it is quite a lot of work, even for simple, small subsystems like
	the kernel-package "make-kpkg" tool.

	Obviously, one can change the subject of the document to suit the NM
	better.  Asking them to convert a technical document they have already
	authored to DocBook within the above requirements would work equally
	well.

	This is the most important item of the doc-NM T&S tests.


2.  Skills at writing technical documents

	Debian isn't about poetry[1], and technical documents have their very
	own characteristics that set them off from usual creative writings.
	Messy documentation is often no better than no documentation at all.

	One is better off by merging this test with the first one.  This is why
	the NM is not asked to just write a filler "lalala llslslssl fooo
	baaar" document with a gnu picture, a table of witty quotes from IRC
	logs, and 42-drunk-penguins-on-the-fence lineart.

	Asking the NM to write either about some part of Debian you know
	nothing about, and always wanted to (or about something you know really
	well) should do the trick.

    [1] No matter how much we would like it to be otherwise.  There is too
	much ugly C, shell and perl code in to call Debian "poetry" ;-)


3.  Skills at summarizing technical documents

	Writing manpages and short summaries are very important skills for a
	documentation developer.  So, one's ability to properly summarize
	things should be tested by the doc NM process.

	"Please write a manpage, using any of the available toolsets (bonus
	points if you can do it in SGML and generate a html version of the
	manpage as well) in Debian stable.

	"The manpage should describe <insert here a section of debian-policy or
	another extended document>.  It must be placed at the proper section
	and subsection (if any) for manpages, and it must have proper
	apropos(1) and whatis(1) behaviour when installed into the system."

	Maybe asking the NM to document a program or utility would be a better
	idea... we have tons of stuff in Debian needing manpages, after all.
	Asking the NM to reduce an info page to a proper manpage is a good
	test as well.


4.  Research skills

	These skills can be probed well enough by a proper choice of subjects
	for the tests above (i.e. ask about something the NM has little
	knowledge of).

	However, should you need to test these skills separately, ask the NM
	to write a short composition about how the LDAP gateway Debian uses
	really work, or about da-katie... or any other obscure part of Debian
	that requires digging mailing lists and CVS code to understand.


5.  Version Control

	It is a must.  A documentation maintainer should know the basics, and
	also about patch, universal diff, merging.  They need not know how to
	setup a CVS server, but they certainly need to know how to use it.

	Ask all of their deliverables to be version-controlled (RCS is ok), and
	request the master archive often, to get a feeling of how good they are
	with the checkins and logging.


6.  Basic Debian-required tools

	GnuPG, email, the BTS, ssh.  If one cannot use them properly, one is
	not ready for Debian.
