#!/usr/bin/env python3
# Copyright (C) 2004  Scott James Remnant <scott@netsplit.com>
# Copyright (C) 2021  Enrico Zini <enrico@enricozini.org>

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR
# ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
# CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


import mailbox
from email.utils import parsedate_tz, mktime_tz
import re
import sys


def print_mail(mail):
    for hdr in ('from', 'to', 'date', 'subject'):
        if hdr in mail:
            print("%s: %s" % (hdr.capitalize(), mail[hdr]))
    print()

    first = True
    for part in mail.walk():
        if not first:
            print("-" * 20)

        if part.get_content_type() == "text/plain":
            print(part.get_payload(decode=True).decode())
            first = False
        elif part.get_content_maintype() != "multipart":
            print("Attachment:")
            print("Content-Type:", part.get_content_type())
            if part.get_filename():
                print("Filename:", part.get_filename())
            first = False


def get_sort_date(mail):
    date = mail["date"]
    if date is None:
        return 0
    parsed = parsedate_tz(date)
    return mktime_tz(parsed)


def read_mbox(filename):
    first = True

    try:
        mbox = mailbox.mbox(filename)

        def strip_re(subject):
            return re.sub(r"^Re:\s*", "", subject)

        mails = list(mbox)
        mails.sort(key=get_sort_date)
        for mail in mails:
            if not first:
                print("-" * 76)
                print()
            first = False

            print_mail(mail)
    finally:
        mbox.close()


if __name__ == "__main__":
    if len(sys.argv) == 1:
        print("Usage: %s <mbox filename>" % sys.argv[0])
        sys.exit(1)
    for filename in sys.argv[1:]:
        read_mbox(filename)
