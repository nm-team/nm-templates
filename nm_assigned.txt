/!\ Template to fill in at the end of the mail (link [0])

I'm happy to say that I have been appointed as your Application
Manager. As such, I will go through the New Member process with
you and then send a summary to the Debian Account Managers (DAM)
and a brief summary to the debian-newmaint mailing list.

First a quick note: please read all of my mails carefully. Some
applicants have a tendency of answering only half the questions. This
generates more work for both of us and means we will need more time
for your NM process.

You may notice that I CC an archive address. Please ensure to always
keep that address copied in your mails as the archive is used in
various steps of the process to make things easier and ensure that
everything goes smoothly.  The archive will remain in the NM database
and it is accessible by all (present and future) AMs, FD and DAM. It's
also readable by yourself here[0].

I'm afraid that these mails are mostly based on pre-formatted
templates which saves time for all concerned but means that some of
the language is a bit stilted and some questions irrelevant.  I'll try
to filter these out but if you see something that doesn't make sense
talk to me about it.

The templates are lacking a bit about me:
I live in <> and maintain <>.
I've been a developer since <>
and my own NM process took <> months.  Hopefully we can beat that :)

At [1] you can see the steps we have to go through. I hope you know
about the basic steps. If not, please read the information at the New
Members Corner[2] and feel free to ask any questions you may have. I'm
here to help you integrate into the project, and I'm happy to help you
in any way I can.

The first step was to sign up to become a Debian Developer and get an
Application Manager (AM) assigned. You have completed the first step.
Now, we are moving on with the AM checks.

Please sign all mail to me. It's not that I'm paranoid about security,
it's just a good habit to get in to, and it shows me that you know
how.


About you
---------

To get started, please tell me about yourself, how you came to
GNU/Linux and free software, and why you want to volunteer your time.
Please describe the contributions you have made to Debian, your primary
areas of interest within Debian, and any goals you wish to accomplish.

You should also ensure that your biography in the NM application website
is up-to-date. When this process is complete it will be sent to a public
mailing list, so you should make sure to remove any parts you don't wish
to publish.


Things to come
--------------

If you have packaged an application for Debian already, please take
another deep look into it, eliminating any bugs you may find. When we
start with the "Tasks and Skills" checks, you will get more information
about the package check and a place where you can upload it.

One thing I should mention: I will ask you many questions in this
process. This is not to torture you, it is to check your knowledge and
to point you in the right directions if you miss something.
After all the questions were developed in the last few
years and every little one has its own reason for being there. Some
aren't easy to solve, but normally a bit of thinking mixed with web
searches and the pointers I give in the various mails should help you, so
I expect you'll be able to answer all of them.

If you're finding any of them hard or you're spending a lot of time
over them then please talk with me so I can give you a hint
or tell you that you've done enough already.  After all, the process
should enhance your knowledge and should not be painful.

You may split your answers to my questions into several mails, this way
we can proceed in smaller steps and you don't need to answer all of
those questions all at once.

I look forward to hearing from you and hope we will be able to go
through the steps without any problems. The next step is "Philosophy
and Procedures" -- I will ask you a few questions by e-mail which you
will need to answer. This will inevitably lead to a discussion about
Debian's goals and core beliefs as well as the way we do things.


Finally, thanks for volunteering! Debian is a volunteer project and
you can make a difference!


Note: If you ever need more than 2 weeks for an answer of a mail (e.g.
if you are on vacation or otherwise busy), please inform me about the
delay. If delays are long or frequent, I may put you on hold until you
have time to complete the process. This is not a bad thing, it just
allows me to take on other applicants during the time you're busy.

If instead for some reason I go missing in action, you can contact Front
Desk at nm@debian.org and they will try to find you a new AM.

[0] https://nm.debian.org/process/${YOUR_PROCESS_ID}/mailbox/
[1] https://wiki.debian.org/DebianDeveloper/JoinTheProject/NewMemberCheckList
[2] https://wiki.debian.org/DebianDeveloper/JoinTheProject/NewMember
