
 * Extra T&S questions

This is an archive of extra T&S questions that can be used by AMs if they feel
they should investigate a bit more.

The idea is that it should be obvious to see whether an applicant already knows
such things, for example by looking at https://wiki.debian.org/DDPortfolio
or minechangelogs https://wiki.debian.org/Services/minechangelogs
or reviewing some of the applicant packages.

But in those cases where there is not much visible activity, yet there are
reasons not to tell the applicant to gather some more experience then come
back, one can find these questions useful.

Debian Package Format
---------------------

PF3. What is the difference between native packages and non-native
     packages?

PF8. Explain the difference between Depends, Recommends, Suggests and
     Enhances.

PF9. What is Pre-depends for? Why should you avoid it?

PFa. Please explain what a virtual package is. Where can you find a
     list of defined virtual packages?

PFb. There is a minimal set of packages you never need to Build-Depend
     on. How do you find which ones? What is the reason for it?

PFc. What is your experience with maintainer scripts? Do you know where
     you can find the full reference of their arguments and their order
     of execution?
     Marga's quick reference can be found here:
     https://wiki.debian.org/MaintainerScripts

PFd. If you had a file in your package which usually gets changed by an
     administrator for local settings, how do you make sure your next
     version of the package doesn't overwrite it?

PFe. How should packages that require special permissions (such as suid
     bits or new local system users) be handled? How do you ensure that
     local permission changes aren't overwritten by upgrades?

Practical packaging
-------------------

PP1. Imagine you maintain a package which depends very closely on some
     other package. How would you keep track of the development of those
     packages, even if you are not the maintainer?

PP2. How can you ensure your package's description is in a good state and
     in a valid format?

PP5. How do you patch upstream sources in a package? What are the
     advantages and disadvantages of doing it? How can you keep the number
     of patches down?

PP7. Please list some good reasons for a package split.

PP8. What would you need to do if upstream changed the name of the
     application? What would you do to assure smooth upgrades?

PPa. What would you do if your package contains an Emacs major mode?
     If you don't use/know Emacs then: What would you do if your
     package contains a Perl (or Python, Ruby, Java, OCaml, Mono...)
     module? Where would you look for more information on that?

PPb. How can you tell your users about some important changes in your
     package?

PPB1. What does automake's AM_MAINTAINER_MODE do? Why is it useful for
      Debian packages? How do you work around it when your package does
      not use it? What do you have to do when you want to enable it in
      your package?

Package Building and Uploading
------------------------------

BU2. What are FTBFS bugs? How can you try to avoid them?

BU3. What is the best way to check that your Build-Depends line contains
     everything required to build your package?

BU4. How do you check a package before you upload? How do you check that
     your package can install, upgrade and remove itself cleanly? Please
     explain to me why and how you perform the checks.

Architectures and Libraries
---------------------------

AL1. What does it mean for a package to have a line like
     "Architecture: i386 alpha powerpc" in the control file?
     What is the difference between "Architecture: all" and
     "Architecture: any"?

AL2. What is an autobuilder? Where can you find information about your
     package's build status on different architectures? What can you do
     if you think there is a problem with the autobuilder?

AL4. Tell me the differences between /usr/share and /usr/lib.
     What would you do if you had a package which includes a mix of
     architecture-dependent and architecture-independent files?

AL6. What is endianness and why does it matter when supporting multiple
     architectures?

AL8. What are library sonames, and what are they used for? What is the
     "ELF" format?

AL9. How does the utility "fakeroot" work? How is that tied to
     LD_PRELOAD, and the runtime linker?

ALB1. What is a symbol-versioned library? Why are libdb5.3 and
      libc6 compiled using symbol versioning? What problems does
      symbol-versioning solve?

ALB2. What is the -Bsymbolic ld flag, exactly what does it do, and how
      does that differ from library symbol versioning? Which problem
      does -Bsymbolic linking solve? Why is libc6 not compiled with
      -Bsymbolic?


Operating System
----------------

OS1. How does Debian handle runlevels? Have you used distributions that
     handle them differently? How does this interact with the user's
     local modifications?  What should packages which require starting up
     and shutting down (like daemons do) do?

OS2. When would you use the alternatives system, and how?

OS3. How can you minimize the downtime of a service during upgrade?
     Be specific about how you'd use the maintainer scripts.

OS4. How does Debian maintain consistency between different window manager
     menus?
